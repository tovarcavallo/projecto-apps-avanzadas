package com.example.projectoappsavanzadas

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import android.widget.Button
import androidx.core.view.GestureDetectorCompat

class MainActivity2 : AppCompatActivity() {
    private lateinit var mDetector: GestureDetectorCompat

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        mDetector = GestureDetectorCompat(this, Swipe2(this.applicationContext,this))
        val buton = findViewById<Button>(R.id.invicible)
        val buton2 = findViewById<Button>(R.id.Volver2)
        val imagen = findViewById<View>(R.id.imageView2)

        buton.setOnClickListener {
            val AnimatorA : ObjectAnimator
            AnimatorA= ObjectAnimator.ofFloat(imagen, View.ALPHA, 1.0f, 0.0f)
            val AnimatorSet1: AnimatorSet = AnimatorSet()

            AnimatorSet1.play(AnimatorA)
            AnimatorSet1.start()

        }



        buton2.setOnClickListener {
            finish()
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)

        }

    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (mDetector.onTouchEvent(event)) {
            return true
        }
        return super.onTouchEvent(event)
    }

}
private class Swipe2(context: Context, activity: MainActivity2) :
        GestureDetector.SimpleOnGestureListener() {
    val contexto = context
    val actividad = activity

    override fun onFling(
            e1: MotionEvent?,
            e2: MotionEvent?,
            velocityX: Float,
            velocityY: Float
    ): Boolean {
        Log.d(DEBUG_TAG, "Fling detectado")
        Log.d(DEBUG_TAG, "Evento inicio: $e1")
        Log.d(DEBUG_TAG, "Evento final: $e2")
        Log.d(DEBUG_TAG, "Velocidad x: $velocityX")
        Log.d(DEBUG_TAG, "Velocidad y: $velocityY")

        var diffX = e1!!.x - e2!!.x
        var diffY = e1!!.y - e2!!.y

        if (Math.abs(diffX) > 200 && Math.abs(diffY) > 200) {
            return true
        }

        if (Math.abs(diffX) > Math.abs(diffY)) {
            //El movimiento es horizontal
            if (diffX > 0) {
                //Swipe left
                this.onSwipeLeft()
            }
            return true
        }
        return super.onFling(e1, e2, velocityX, velocityY)
    }


    private fun onSwipeLeft() {
        Log.d(DEBUG_TAG, "onSwipeLeft")
        actividad.finish()
        actividad.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)

    }




}
