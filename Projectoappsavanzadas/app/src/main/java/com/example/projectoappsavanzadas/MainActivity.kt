package com.example.projectoappsavanzadas

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.media.Image
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Debug
import android.os.strictmode.IntentReceiverLeakedViolation
import android.provider.MediaStore
import android.util.Log
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import android.widget.Button
import android.widget.ImageButton
import android.widget.Toast
import android.widget.VideoView
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat.startActivityForResult
import androidx.core.view.GestureDetectorCompat

const val DEBUG_TAG= "GESTOS"
const val REQUEST_VIDEO_CAPTURE =2
private lateinit var mDetector: GestureDetectorCompat
private lateinit var sensorManager: SensorManager
private var sensorAceleracion: Sensor? = null
private var Aceleracion = 0f
private var AceleracionActual = 0f
private var ultimaAceleracion = 0f
var datap : Intent? = null



class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager


        mDetector = GestureDetectorCompat(this, Swipe(this.applicationContext, this))
        val buton = findViewById<Button>(R.id.boton)
        val buton2 = findViewById<Button>(R.id.boton1)
        val cerrar = findViewById<ImageButton>(R.id.Cerrar)




        buton.setOnClickListener {
            val llamada: Intent = Intent(this, MainActivity2::class.java)
            startActivityForResult(llamada, 0)
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)

        }
        buton2.setOnClickListener {
            val llamada: Intent = Intent(this, MainActivity3::class.java)
            startActivityForResult(llamada, 0)
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
        }
        cerrar.setOnClickListener {
            val constructor = AlertDialog.Builder(this)
            constructor.setTitle("Cerrar")
                    .setMessage("¿Estas Seguro de que quieres Salir?")
                    .setPositiveButton(
                            "Si",
                            DialogInterface.OnClickListener { dialog, which -> this.finish() }
                    )
            constructor.show()
        }

        Aceleracion = 10f
        AceleracionActual = SensorManager.GRAVITY_EARTH
        ultimaAceleracion = SensorManager.GRAVITY_EARTH


    }


    private val EventoAceleracion: SensorEventListener = object : SensorEventListener {
        override fun onSensorChanged(event: SensorEvent?) {


            val x = event!!.values[0]
            val y = event!!.values[1]
            val z = event!!.values[2]
            ultimaAceleracion = AceleracionActual
            AceleracionActual = Math.sqrt((x * x + y * y + z * z).toDouble()).toFloat()
            val delta = AceleracionActual - ultimaAceleracion
            Aceleracion = Aceleracion * 0.9f + delta
            if (Aceleracion > 20) {
                val intentHacerVideo = Intent(MediaStore.ACTION_VIDEO_CAPTURE)
                if (intentHacerVideo.resolveActivity(packageManager) != null) {
                    onPause()
                    startActivityForResult(intentHacerVideo, REQUEST_VIDEO_CAPTURE)


                } else {
                    Toast.makeText(applicationContext, "No hay app de vídeo", Toast.LENGTH_SHORT).show()
                }
            }

        }


        override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {

        }

    }

    override fun onResume() {

        sensorManager.registerListener(EventoAceleracion, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL)
        super.onResume()

    }

    override fun onPause() {
        sensorManager.unregisterListener(EventoAceleracion)

        super.onPause()
    }


    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (mDetector.onTouchEvent(event)) {
            return true
        }
        return super.onTouchEvent(event)
    }





    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode.equals(REQUEST_VIDEO_CAPTURE)){
            datap = data
            val inte :Intent = Intent(this, MainActivity4::class.java)



            val constructor = AlertDialog.Builder(this)
            constructor.setTitle("Video")
                    .setMessage("Se le mostrara el Vídeo")
                    //Abrirá Activity 4
                    .setPositiveButton(
                            "Ok",
                            DialogInterface.OnClickListener{ dialog, which -> startActivity(inte) }
                    )

            constructor.show()
        }



        super.onActivityResult(requestCode, resultCode, data)


    }

}
private class Swipe(context: Context, activity: MainActivity) :
    GestureDetector.SimpleOnGestureListener() {
    val contexto = context
    val actividad = activity

    override fun onFling(
        e1: MotionEvent?,
        e2: MotionEvent?,
        velocityX: Float,
        velocityY: Float
    ): Boolean {
        Log.d(DEBUG_TAG, "Fling detectado")
        Log.d(DEBUG_TAG, "Evento inicio: $e1")
        Log.d(DEBUG_TAG, "Evento final: $e2")
        Log.d(DEBUG_TAG, "Velocidad x: $velocityX")
        Log.d(DEBUG_TAG, "Velocidad y: $velocityY")

        var diffX = e1!!.x - e2!!.x
        var diffY = e1!!.y - e2!!.y

        if (Math.abs(diffX) > 200 && Math.abs(diffY) > 200) {
            return true
        }

        if (Math.abs(diffX) > Math.abs(diffY)) {
            //El movimiento es horizontal
            if (diffX > 0) {
                //Swipe left
                this.onSwipeLeft()
            } else {
                //Swipe right
                this.onSwipeRigth()
            }
            return true
        }


        return super.onFling(e1, e2, velocityX, velocityY)
    }


    private fun onSwipeLeft() {
        Log.d(DEBUG_TAG, "onSwipeLeft")
        val inte : Intent = Intent(contexto, MainActivity3::class.java)
        actividad.startActivityForResult(inte,0)
        actividad.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)

    }



    private fun onSwipeRigth() {
        Log.d(DEBUG_TAG, "onSwipeRigth")
        val inte : Intent = Intent(contexto, MainActivity2::class.java)
        actividad.startActivityForResult(inte,0)
        actividad.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }




}



